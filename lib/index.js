class Whatsapp {
  constructor ({ user, pass, url, api_version = 'v1' }) {
    this.credential = Buffer.from(`${user}:${pass}`).toString('base64')
    this.url = url
    this.api_version = api_version
    this.token = null

    this._contacts = require('./modules/contacts')
    this._users = require('./modules/users')
    this._health = require('./modules/health')
    this._messages = require('./modules/messages')
    this._media = require('./modules/media')
    this.utils = require('./modules/functions')
  }

  async _login () {
    const { users: [{ token }] } = await this._users.login(this)
    this.token = token
  }

  async checkContacts ({ contacts }) {
    if (!this.token) await this._login()
    return this._contacts.check({ ...this, ...{ contacts } })
  }

  async sendMessage ({ to, audio, document, video, text, image, type, caption }) {
    if (!this.token) await this._login()

    return this._messages.send.individual({ ...this, ...{ to, audio, document, video, text, image, type, caption } })
  }

  async sendTemplateMessage ({ to, namespace, element_name, localizable_params }) {
    if (!this.token) await this._login()

    return this._messages.send.template({ ...this, ...{ to, namespace, element_name, localizable_params } })
  }

  async uploadMedia ({ file }) {
    if (!this.token) await this._login()

    return this._media.upload({ ...this, ...{ file } })
  }

  async findMedia ({ id }) {
    if (!this.token) await this._login()

    return this._media.search({ ...this, ...{ id } })
  }
}

module.exports = Whatsapp
