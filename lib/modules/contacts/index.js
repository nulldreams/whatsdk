const request = require('../../utils/request')
const to = require('await-to-js').default
const route = 'contacts'

const check = async ({ url, token, contacts, api_version }) => {
  const headers = { Authorization: `Bearer ${token}`, 'Content-Type': 'application/json' }

  const [err, response] = await to(request.post({ url: `${url}/${api_version}/${route}`, headers, data: { blocking: 'wait', contacts } }))

  if (err) return err.response.data

  return response.data
}

module.exports = {
  check
}
