const request = require('../../utils/request')
const _to = require('await-to-js').default

const send = {
  individual: async ({ url, token, to, type, audio, document, video, text, image, api_version, caption }) => {
    const headers = { Authorization: `Bearer ${token}`, 'Content-Type': 'application/json' }

    let data = { recipient_type: 'individual', to, type, audio, document, video, text, image, caption }

    data = onlyWithValue({ ...data, ...handlePayloadType(data) })

    const [err, response] = await _to(request.post({ url: `${url}/${api_version}/messages`, headers, data }))

    if (err) return err.response.data

    return response.data
  },
  template: async ({ url, token, api_version, to, namespace, element_name, localizable_params }) => {
    const headers = { Authorization: `Bearer ${token}`, 'Content-Type': 'application/json' }

    // console.log('TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT', { url, token, api_version, to, namespace, element_name, localizable_params })
    const data = {
      to,
      type: 'hsm',
      hsm: {
        namespace,
        element_name,
        language: {
          policy: 'deterministic',
          code: 'pt_BR'
        },
        localizable_params
      }
    }

    const [err, response] = await _to(request.post({ url: `${url}/${api_version}/messages`, headers, data }))

    // console.log('Cluster response: ', response, 'Cluster error:', err)

    if (err) return err.response.data

    return response.data
  },
  setMessageAsRead: async ({ url, token, messageId, api_version }) => {
    const headers = { Authorization: `Bearer ${token}`, 'Content-Type': 'application/json' }

    const data = { status: 'read' }

    const [err, response] = await _to(request.put({ url: `${url}/${api_version}/messages/${messageId}`, headers, data }))

    if (err) return err.response.data

    return response.data
  }
}

function handlePayloadType ({ audio, document, video, text, image, caption }) {
  const types = onlyWithValue({ audio, document, video, text, image })
  const payload = {}

  for (const type in types) {
    if (type === 'text') payload.text = { body: types[type] }
    if (type === 'audio') payload.audio = { link: audio }
    if (type === 'image') payload.image = { link: image, caption }
    if (type === 'video') payload.video = { link: video, caption }
    if (type === 'document') payload.document = { link: document, caption }
  }

  return payload
}

function onlyWithValue (obj) {
  Object.keys(obj).map(v => obj[v] === undefined ? delete obj[v] : '')
  return obj
}

module.exports = {
  send
}
