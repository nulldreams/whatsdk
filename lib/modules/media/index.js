const request = require('../../utils/request')
const _to = require('await-to-js').default
const fs = require('fs')
const req = require('request')

const upload = async ({ url, token, api_version, file }) => {
  const data = await fs.readFileSync(file, { encoding: 'utf8' })

  const headers = {
    Authorization: `Bearer ${token}`,
    'Content-Type': 'video/mp4',
    'Content-Length': data.length
  }

  const [err, response] = await _to(request.post({ url: `${url}/${api_version}/media`, headers, data }))

  if (err) return err.response.data

  return response.data
}

const search = async ({ url, token, api_version, id }) => {
  const options = {
    method: 'GET',
    url: `${url}/${api_version}/media/${id}`,
    headers: {
      Authorization: `Bearer ${token}`
    },
    encoding: null
  }

  return new Promise((resolve, reject) => {
    req(options, function (error, response, body) {
      if (error) return reject(error)

      return resolve(body)
    })
  })
}

module.exports = {
  upload,
  search
}
