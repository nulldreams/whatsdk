const request = require('../../utils/request')
const to = require('await-to-js').default
// const { checkCredentials, userToken } = require('../../utils/credentials')
const route = 'health'

const check = async () => {
  // await checkCredentials()

  // const headers = { Authorization: `Bearer ${await userToken()}`, 'Content-Type': 'application/json' }

  const [err, response] = await to(request.get({ route: `${route}` }))

  if (err) return err.response.data

  return response.data
}

module.exports = {
  check
}
