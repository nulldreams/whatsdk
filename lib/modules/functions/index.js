const validContacts = (contacts) => {
  return contacts.filter(c => {
    return c.status === 'valid'
  })
}

const invalidContacts = (contacts) => {
  return contacts.filter(c => {
    return c.status === 'invalid'
  })
}

module.exports = {
  validContacts,
  invalidContacts
}
