const request = require('../../utils/request')
const to = require('await-to-js').default

const login = async ({ url, credential, api_version }) => {
  const headers = {
    Authorization: `Basic ${credential}`,
    'Content-Type': 'application/json',
    host: url.replace('https://', '')
  }

  const [err, response] = await to(request.post({ url: `${url}/${api_version}/users/login`, headers }))

  if (err) return err.response.data

  return response.data
}

module.exports = {
  login
}
