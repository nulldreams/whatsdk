const config = require('../config')
const axios = require('axios')

const get = ({ route, headers }) => {
  headers = { ...headers, host: config.api.host }
  return axios({ method: 'get', url: `${config.api.url}/${config.api.version}${route}`, headers })
}

const post = ({ url, headers, data }) => {
  return axios({ method: 'post', url, headers, data })
}

const put = ({ url, headers, data }) => {
  return axios({ method: 'put', url, headers, data })
}

module.exports = { get, post, put }
