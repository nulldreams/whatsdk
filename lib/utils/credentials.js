const moment = require('moment')
const path = require('path')
const fs = require('fs')

const credentialPath = path.resolve(__dirname, '../config/', 'credentials.json')

const checkCredentials = async () => {
  if (!fs.existsSync(credentialPath)) return { valid: false }

  const credential = JSON.parse(await fs.readFileSync(credentialPath, 'utf-8'))

  if (isInvalidToken(credential)) return { valid: false }

  return { valid: true, token: await userToken() }
}

async function userToken () {
  const credential = JSON.parse(await fs.readFileSync(credentialPath, 'utf-8'))

  return credential.token
}

const saveCredentials = async ({ users: [{ token, expires_after: expires }] }) => {
  const data = JSON.stringify({ token, expires_after: expires }, null, 2)

  await fs.writeFileSync(credentialPath, data, 'utf-8')
}

function isInvalidToken ({ expires_after, token }) {
  const expires_date = moment(expires_after)
  const today = moment()
  const diff = expires_date.diff(today, 'days')

  return diff === 0 || diff > 7 || !token
}

module.exports = {
  checkCredentials,
  saveCredentials
}
